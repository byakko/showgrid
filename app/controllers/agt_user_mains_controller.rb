class AgtUserMainsController < ApplicationController

  def index
    condition = AgtUserMain.exist
    @total_length = condition.count
    condition = condition.skip((params[:page].to_i - 1) * 10) if params[:page]
  	@agt_users = condition.limit(10).all
  end

  def show
    @agt_user = AgtUserMain.exist.where(:PKID => params[:id]).first
  end

  def create
    agt_user_main = AgtUserMain.new(params.slice(:UserName, :Agentname, :Fax, :City, :IsActive))
    agt_user_main.sys_province = SysProvince.where(ZIP: params[:Province]).first
    agt_user_main.save!
    render :json => {
      success: true
    }
  end

  def update
    agt_user_main = AgtUserMain.exist.where(:PKID => params[:id]).first
    agt_user_main.update_attributes!(params.slice(:UserName, :Agentname, :Fax, :City, :IsActive))
    agt_user_main.sys_province = SysProvince.where(ZIP: params[:Province]).first
    agt_user_main.save
    render :json => {
      success: true
    }
  end

  def destroy
    agt_user = AgtUserMain.exist.where(:PKID => params[:id]).first
    agt_user.IsDel = true
    agt_user.save
    render :json => {
      success: true
    }
  end

  def resetPwd
    agt_user = AgtUserMain.exist.where(:PKID => params[:id]).first
    agt_user.Password = '777777'
    agt_user.save
    render :json => {
      success: true
    }
  end

  def valid
    response = {
      isVaild: true
    }
    case params[:property]
    when 'UserName'
      condition = AgtUserMain.where(:UserName => params[:value])
      condition = condition.for_js("this.PKID != id", id: params[:id]) if params[:id]
      if condition.first
        response.merge!({
          isVaild: false,
          message: '用户名已存在'
        })
      end
    end
    render :json => response
  end
end
