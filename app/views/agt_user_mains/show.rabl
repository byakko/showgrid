object @agt_user => :agt_user

attributes :PKID, :UserName, :Agentname, :Fax, :City, :IsActive, :Createtime

node(:Province, :if => lambda { |u| u.sys_province }) do |u|
  u.sys_province.ZIP
end