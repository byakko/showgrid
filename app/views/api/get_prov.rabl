collection @provs, :object_root => false

attributes :ZIP, :Name

child :sys_cities, :object_root => false do
  attributes :ZIP, :Name
end