class SysArea
  include Mongoid::Document
  field :ZIP, type: Integer
  field :Name, type: String
  embedded_in :sys_city
end