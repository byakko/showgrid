class SysProvince
  include Mongoid::Document
  field :ZIP, type: Integer
  field :Name, type: String

  embeds_many :sys_cities

  has_many :agt_user_mains
end