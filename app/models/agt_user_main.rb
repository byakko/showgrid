class AgtUserMain
  include Mongoid::Document
  field :PKID, type: String, default: ->{ SecureRandom.uuid }
  field :UserName, type: String
  field :Password, type: String, default: "666666"
  field :Agentname, type: String
  field :Fax, type: String
  field :City, type: Integer
  field :Status, type: Integer, default: 0
  field :IsDel, type: Boolean, default: false
  field :IsActive, type: Boolean
  field :Createtime, type: DateTime, default: ->{ DateTime.now }

  scope :exist, ->{ where(:IsDel => false) }

  belongs_to :sys_province
  validates :UserName, uniqueness: true
end