class SysCity
  include Mongoid::Document
  field :ZIP, type: Integer
  field :Name, type: String

  embedded_in :sys_province
  embeds_many :sys_areas
end