define [
	'knockout'
	'jquery'
], (ko, $) ->
	'use strict'

	class User
		constructor: (data) ->
			@PKID = ko.observable(data.PKID)
			@UserName = ko.observable(data.UserName)
			@Agentname = ko.observable(data.Agentname)
			@Fax = ko.observable(data.Fax)
			@Province = ko.observable(data.Province)
			@City = ko.observable(data.City)
			@IsActive = ko.observable(data.IsActive)
			@Createtime = ko.observable(data.Createtime)
			@errors = ko.validation.group(@)

		formatTime: -> new Date(@Createtime()).toLocaleString()
		formatIsActive: -> if @IsActive() then '是' else '否'

		activeVaild: ->
			@UserName.extend
				required:
					message: '请输入用户名'
				maxLength:
					params: 20
					message: '用户名过长'
			@UserName.extend(
				remote:
					params:
						url: '/agt_user_mains/valid'
						property: 'UserName'
			) unless @PKID()
			@Agentname.extend
				maxLength:
					params: 20
					message: '代理商名称过长'

		add: (callback) ->
			requestData = JSON.parse(ko.toJSON(@))
			return if requestData.errors.length
			$.ajax '/agt_user_mains', data: requestData, type: 'POST', complete: (data, status) -> callback.call(@, data, status)

		update: (callback) ->
			requestData = JSON.parse(ko.toJSON(@))
			return if requestData.errors.length
			$.ajax '/agt_user_mains/' + requestData.PKID, data: requestData, type: 'PUT', complete: (data, status) -> callback.call(@, data, status)

		resetPwd: (callback) ->
			$.ajax '/agt_user_mains/' + @PKID() + '/resetPwd', type: 'PUT', complete: (data, status) -> callback.call(@, data, status)

		delete: (callback) ->
			$.ajax '/agt_user_mains/' + @PKID(), type: 'DELETE', complete: (data, status) -> callback.call(@, data, status)