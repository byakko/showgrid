app.Model = app.Model or {}

app.Model.User = class
	constructor: (data) ->
		@help = new app.Shared.ModelHelp @
		@help.init(data)

		@formatTime = ko.computed => new Date(@Createtime()).toLocaleString()
		@formatIsActive = ko.computed => if @IsActive() then '是' else '否'

		@UserName = @UserName.extend
			throttle: 500

		@errors = ko.validation.group(@)

	key: 'PKID'
	url: '/agt_user_mains'
	persist: [
		'UserName'
		'Agentname'
		'Fax'
		'Province'
		'City'
		'IsActive'
		'Createtime'
	]

	activeVaild: ->
		@UserName.extend
			required:
				message: '请输入用户名'
			maxLength:
				params: 20
				message: '用户名过长'
		@UserName.extend(
			remote:
				params:
					url: @url
					property: 'UserName'
					id: @_key or ''
		)
		@Agentname.extend
			maxLength:
				params: 20
				message: '代理商名称过长'

	delete: (callback) ->
		@help.delete().done callback

	resetPwd: (callback) ->
		$.ajax(@help.requestUrl() + '?resetPwd=true', type: 'PUT').done callback