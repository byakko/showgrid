app.Model = app.Model or {}

app.Model.UserCollection = class
	constructor: (@parent) ->
		@help = new app.Shared.CollectionHelp @
		@help.init()

		@userBench = ko.observable()
		@benchType = ko.observable()

		@provs = ko.observableArray []
		@cities = ko.observableArray []

		@provLoad()

		@pagination = new app.Shared.Pagination @

	model: app.Model.User
	dataRoot: 'agt_users'

	add: ->
		user = @help.new {}
		@provChange(user)
		@userBench user
		user.activeVaild()
		@benchType 'create'

	update: (user) ->
		user.help.fetch().done (data) =>
			user = @help.new data.agt_user
			@provChange(user)
			@userBench user
			user.activeVaild()
			@benchType 'update'

	submit: (user) ->
		switch @benchType()
			when 'create'
				user.help.saveChanges().done (data, status) =>
					if status == 'success'
						@parent.alert '用户已添加', 'success'
						@pagination.reload()
					else
						@alertError()
			when 'update'
				user.help.saveChanges().done (data, status) =>
					if status == 'success'
						@parent.alert '修改成功', 'success'
						@pagination.reload()
					else
						@parent.alertError()

	resetPwd: (user) ->
		@parent.confirmTask.push user.resetPwd, user, (data, status) =>
			@parent.alert '密码已重置', 'success' if status == 'success'

	delete: (user) ->
		@parent.confirmTask.push user.delete, user, (data, status) =>
			@parent.alert '删除成功', 'success' if status == 'success'
			@pagination.reload()

	provLoad: ->
		$.getJSON "/api/get_prov", (provs) =>
			@provs provs

	provChange: (user) ->
		selectedProv = ko.utils.arrayFilter(@provs(), (prov) ->
			prov.ZIP == user.Province()
		)[0]
		cities = if selectedProv then selectedProv.sys_cities else []
		@cities cities