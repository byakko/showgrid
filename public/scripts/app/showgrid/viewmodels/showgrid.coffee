app.ViewModel = app.ViewModel or {}

app.ViewModel.ShowGrid = class
	constructor: ->
		@contentAlert = ko.observable()
		@typeAlert = ko.observable()

		@confirmTask = new app.Shared.ConfirmTask

		@userCollection = new app.Model.UserCollection @

		@alert = (content, type) =>
			@contentAlert(content)
			@typeAlert('alert-' + type)

		@alertError = =>
			@alert '服务器异常', 'error'

		@cleanAlert = =>
			@alert undefined, 'info'

		@flush = =>
			@confirmTask.flush()

		@cancel = =>
			@confirmTask.cancel()

		@userCollection.help.load
			page: 1