app = app || {}
app.Shared = app.Shared or {}

app.Shared.Pagination = class
	constructor: (@collection) ->
		@totalPage = ko.computed =>
			Math.ceil(@collection.totalLength() / 10)
		@currentPage = ko.observable(1)
		@pages = ko.observableArray []

		@totalPage.subscribe @pageInit, @

	changePage: (page) ->
		return if page.index == 0
		@currentPage page.index
		@reload()
		@pageInit()

	reload: () ->
		@collection.help.load
			page: @currentPage()

	pageInit: ->
		currentPage = @currentPage()
		totalPage = if @totalPage() then @totalPage() else 1
		newPages = [{
			content: currentPage
			index: currentPage
		}]
		padding =
			content: '...'
			index: 0
		headPage =
			content: '«'
			index: 1
		tailPage =
			content: '»'
			index: totalPage
		head = 1
		tail = totalPage
		count = 5

		forward = 'tail'

		cursors =
			head:
				index: currentPage
				step: -1
				reached: false
			tail:
				index: currentPage
				step: 1
				reached: false

		add = ->
			return if newPages.length >= count
			newIndex = cursors[forward].index + cursors[forward].step
			if newIndex > tail or newIndex < head
				cursors[forward].reached = true
				return if cursors['head'].reached and cursors['tail'].reached
			else
				location = if forward == 'head' then 0 else newPages.length
				newPages.splice(location, 0, {
					content: newIndex
					index: newIndex
				})
				cursors[forward].index = newIndex
			forward = if forward == 'head' then 'tail' else 'head'
			add()

		add()
		newPages.push padding unless tail <= cursors.tail.index
		newPages.unshift padding unless head >= cursors.head.index
		newPages.push tailPage
		newPages.unshift headPage
		@pages newPages
