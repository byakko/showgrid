(() ->
	validation = ko.validation
	validation.init
		errorMessageClass: 'help-block'

	validation.rules.remote =
		async: true
		validator: (value, params, callback) ->
			requestData = {
				property: params.params.property
				value: value
				id: params.params.id
			}
			options =
				url: params.params.url + '/valid'
				data: requestData
				success: (response) ->
					callback(response)

			$.ajax options

	validation.registerExtenders()
)()