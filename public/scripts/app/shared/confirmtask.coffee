app = app or {}
app.Shared = app.Shared or {}

app.Shared.ConfirmTask = class
	constructor: ->
		@cache = []
	push: (fnc, obj, callback) ->
		@cache.push {
			fnc: fnc
			obj: obj
			callback: callback
		}
	flush: ->
		for task in @cache
			do (task) -> task.fnc.call(task.obj, task.callback)
		@cache.length = 0
	cancel: ->
		@cache.length = 0