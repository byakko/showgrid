app = app or {}
app.Shared = app.Shared or {}

app.Shared.CollectionHelp = class
	constructor: (@collecion) ->

	init: (data = []) ->
		@collecion.totalLength = ko.observable()
		@collecion.data = ko.observableArray []
		@reset data

	load: (params) ->
		$.getJSON @collecion.model.prototype.url, params, (allData) =>
			@reset allData[@collecion.dataRoot]
			@collecion.totalLength allData.totalLength

	new: (data) ->
		new @collecion.model(data)

	reset: (data) ->
		mappedModels = $.map data, (item) => new @collecion.model(item)
		@collecion.data mappedModels
