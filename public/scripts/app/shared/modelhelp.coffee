app = app or {}
app.Shared = app.Shared or {}

app.Shared.ModelHelp = class
	constructor: (@model) ->

	init: (data) ->
		@model['_key'] = data[@model['key']]
		ko.utils.arrayForEach @model.persist, (attr) =>
			@model[attr] = ko.observable(data[attr])

	requestUrl: ->
		if @model['_key'] then @model.url + '/' + @model['_key'] else @model.url

	fetch: ->
		$.ajax @requestUrl(), type: 'GET'

	saveChanges: ->
		persistObject = {}
		ko.utils.objectForEach @model, (name, value) =>
			if ko.utils.arrayIndexOf(@model.persist, name) != -1
				persistObject[name] = value

		options = {
			data: ko.toJS(persistObject)
			url: @requestUrl()
			type: if @model['_key'] then 'PUT' else 'POST'
		}

		$.ajax options

	delete: ->
		$.ajax @requestUrl(), type: 'DELETE'