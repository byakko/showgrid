define [
	'knockout'
	'jquery'
	'models/User'
], (ko, $, User) ->
	'use strict'

	class ConfirmTask
		constructor: ->
			@cache = []
		push: (fnc, obj, callback) ->
			@cache.push {
				fnc: fnc
				obj: obj
				callback: callback
			}
		flush: ->
			for task in @cache
				do (task) -> task.fnc.call(task.obj, task.callback)
			@cache.length = 0
		cancel: ->
			@cache.length = 0

	confirmTask = new ConfirmTask

	class ViewModel
		constructor: ->
			@users = ko.observableArray []

			@userBench = ko.observable()
			@benchType = ko.observable()

			@totalPage = ko.observable()
			@currentPage = ko.observable(1)
			@pages = ko.observableArray []

			@contentAlert = ko.observable()
			@typeAlert = ko.observable()

			@provs = ko.observableArray()
			@cities = ko.observableArray []

			@alert = (content, type) =>
				@contentAlert(content)
				@typeAlert('alert-' + type)

			@alertError = =>
				@alert '服务器异常', 'error'

			@cleanAlert = =>
				@alert undefined, 'info'

			@add = =>
				user = new User {}
				@provChange(user)
				@userBench user
				user.activeVaild()
				@benchType 'create'

			@update = (user) =>
				$.getJSON "/agt_user_mains/" + user.PKID(), (data) =>
					user = new User data.agt_user
					@provChange(user)
					@userBench user
					user.activeVaild()
					@benchType 'update'

			@submit = (user) =>
				switch @benchType()
					when 'create'
						user.add (data, status) =>
							if status == 'success'
								@alert '用户已添加', 'success'
								@load()
							else
								@alertError()
					when 'update'
						user.update (data, status) =>
							if status == 'success'
								@alert '修改成功', 'success'
								@load()
							else
								@alertError()

			@resetPwd = (user) =>
				confirmTask.push(user.resetPwd, user, (data, status) =>
					@alert '密码已重置', 'success' if status == 'success')

			@delete = (user) =>
				confirmTask.push(user.delete, user, (data, status) =>
					@alert '删除成功', 'success' if status == 'success'
					@load())

			@flush = ->
				confirmTask.flush()

			@cancel = ->
				confirmTask.cancel()

			@load = =>
				$.getJSON "/agt_user_mains", page: @currentPage(), (allData) =>
			        mappedUsers = $.map allData.agt_users, (item) -> new User(item)
			        @users mappedUsers
			        @pageInit Math.ceil(allData.totalLength / 10)

			@pageInit = (totalPage) =>
				return if @totalPage() == totalPage
				@totalPage (totalPage)
				@pageRender()

			@pageRender = =>
				currentPage = @currentPage()
				totalPage = @totalPage()
				newPages = [{
					content: currentPage
					index: currentPage
				}]
				padding =
					content: '...'
					index: 0
				headPage =
					content: '«'
					index: 1
				tailPage =
					content: '»'
					index: totalPage
				head = 1
				tail = totalPage
				count = 5

				forward = 'tail'

				cursors =
					head:
						index: currentPage
						step: -1
						reached: false
					tail:
						index: currentPage
						step: 1
						reached: false

				add = ->
					return if newPages.length >= count
					newIndex = cursors[forward].index + cursors[forward].step
					if newIndex > tail or newIndex < head
						cursors[forward].reached = true
						return if cursors['head'].reached and cursors['tail'].reached
					else
						location = if forward == 'head' then 0 else newPages.length
						newPages.splice(location, 0, {
							content: newIndex
							index: newIndex
						})
						cursors[forward].index = newIndex
					forward = if forward == 'head' then 'tail' else 'head'
					add()

				add()
				newPages.push padding unless tail == cursors.tail.index
				newPages.unshift padding unless head == cursors.head.index
				newPages.push tailPage
				newPages.unshift headPage
				@pages newPages

			@changePage = (page) =>
				return if page.index == 0
				@currentPage page.index
				@load()
				@pageRender()

			@provLoad = =>
				$.getJSON "/api/get_prov", (provs) =>
			        @provs provs

	        @provChange = (user) =>
	        	selectedProv = ko.utils.arrayFilter(@provs(), (prov) ->
	        		prov.ZIP == user.Province()
        		)[0]
        		cities = if selectedProv then selectedProv.sys_cities else []
	        	@cities cities

			@load()
			@provLoad()