require.config
	paths:
		knockout: 'lib/knockout-2.3.0'
		jquery: 'lib/jquery-2.0.3.min'
		'knockout.validation': 'lib/knockout.validation.min'
		bootstrap: 'lib/bootstrap.min'
	shim:
		'knockout.validation': ['knockout']
		bootstrap: ['jquery']

require [
	'knockout'
	'jquery'
	'knockout.validation'
	'bootstrap'
	'viewmodels/showgrid'
], (ko, $, validation, bootstrap, ShowGridViewModel) ->
		'use strict'
		validation.init
			errorMessageClass: 'help-block'

		validation.rules.remote =
			async: true
			validator: (value, params, callback) ->
				requestData = {}
				requestData[params.params.property] = value
				options =
					data: requestData
					success: (response) ->
						callback(response)

				$.ajax params.params.url, options

		validation.registerExtenders()

		ko.applyBindings new ShowGridViewModel