namespace :mytasks do
  desc "将省份文件中的数据转移到数据库"
  task :prov_migrate, [:path] => :environment do |t, args|
    line_num = 0
    File.foreach(args[:path]) { |line|
      line_num += 1
      case
      when line_num < 125
        meta = /\((?<zip>\d+), \'(?<name>.+)\'\)/.match(line)
        SysProvince.where({
          	ZIP: meta[:zip].to_i,
          	Name: meta[:name]
      	}).first_or_create if meta
      when line_num < 475
      	meta = /\((?<zip>\d+),\s*[\'\"](?<name>.+)[\'\"],\s*(?<parent_code>\d+)\)/.match(line)
        if meta
			sys_prov = SysProvince.where(ZIP: meta[:parent_code]).first
			sys_prov.sys_cities.find_or_create_by({
			  ZIP: meta[:zip],
			  Name: meta[:name]
			}) if sys_prov
        end
      else
      	meta = /\((?<zip>\d+),\s*[\'\"](?<name>.+)[\'\"],\s*(?<parent_code>\d+)\)/.match(line)
        if meta
			sys_prov = SysProvince.where(:sys_cities.elem_match => { ZIP: meta[:parent_code].to_i }).first
			if sys_prov
				sys_prov.sys_cities.where(ZIP: meta[:parent_code]).first.sys_areas.find_or_create_by({
				  ZIP: meta[:zip],
				  Name: meta[:name]
				})
			end
        end
      end
    }
  end
end